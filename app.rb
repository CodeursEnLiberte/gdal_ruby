require 'sinatra'
require 'open3'

set :port, ENV['PORT'] || 4567

get '/' do
  # /vsistdout/ is  a special file to output to stdout https://gdal.org/user/virtual_file_systems.html#vsistdout-standard-output-streaming
  # 'EPSG:4326' -t_srs makes sure that everything is reprojected to 4326 (usual GPS coordinates)
  stdout, stderr, status = Open3.capture3('ogr2ogr', '-f', 'geojson', '-t_srs', 'EPSG:4326', '/vsistdout/', "WFS:#{params['wfs']}", params['layer'])
  if status.success?
    puts "succes !!"
    stdout
  else
    'Couldn’t convert: ' + stderr
  end
end
